{
  const code = `
    .preview-wrapper {
      display: flex;
      justify-content: center;
      align-items: center;
      background: #ffe600;
    }
    /* 画板 */
    .canvas {
      position: relative;
      /* border: 1px solid black; */
      height: 350px;
      width: 460px;
    }
    /* 鼻子 */
    .nose {
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
      top: 100px;
      border: 10px solid transparent;
      border-top-color: black;
    }
    .nose::before {
      content: '';
      position: absolute;
      top: -15px;
      left: -10px;
      width: 20px;
      height: 5px;
      background: black;
      border-top-right-radius: 10px 5px;
      border-top-left-radius: 10px 5px;
    }
    /* 眼睛 */
    .eye {
      position: absolute;
      top: 45px;
      width: 60px;
      height: 60px;
      background: #2e2e2e;
      border: 2px solid black;
      border-radius: 50%;
    }
    .eye::before {
      content: '';
      display: block;
      width: 28px;
      height: 28px;
      border-radius: 14px;
      position: absolute;
      top: 1px;
      left: 8px;
      background: white;
      border: 2px solid black;
    }
    .eye-left {
      left: 50%;
      margin-left: 70px;
    }
    .eye-right {
      right: 50%;
      margin-right: 70px;
    }
    /* 脸上的红晕 */
    .face {
      position: absolute;
      top: 160px;
      width: 90px;
      height: 90px;
      border-radius: 45px;
      border: 2px solid black;
      background: #ff0000;
    }
    .face-left {
      left: 50%;
      margin-left: 140px;
    }
    .face-right {
      right: 50%;
      margin-right: 140px;
    }
    /* 嘴巴 */
    .upperLip {
      position: absolute;
      top: 129px;
      width: 100%;
      /* border: 1px solid red; */
      display: flex;
      justify-content: center;
    }
    .upperLip::before {
      content: '';
      display: block;
      width: 80px;
      height: 21px;
      background: #ffe600;
      border: 3px solid black;
      border-top: none;
      border-right: none;
      border-bottom-left-radius: 40px 20px;
      transform: rotateZ(-20deg);
      z-index: 1;
    }
    .upperLip::after {
      content: '';
      display: block;
      width: 80px;
      height: 21px;
      background: #ffe600;
      border: 3px solid black;
      border-top: none;
      border-left: none;
      border-bottom-right-radius: 40px 20px;
      transform: rotateZ(20deg);
      z-index: 1;
    }
    .lowerLip-wrapper {
      position: absolute;
      /* border: 1px solid red; */
      width: 100%;
      height: 155px;
      top: 136px;
      overflow: hidden;
    }
    .lowerLip {
      position: absolute;
      border: 3px solid black;
      width: 135px;
      height: 600px;
      top: -445px;
      left: 50%;
      transform: translateX(-50%);
      border-bottom-left-radius: 65px 300px;
      border-bottom-right-radius: 65px 300px;
      border-top-left-radius: 65px 45px;
      border-top-right-radius: 65px 45px;
      background: #ff485f;
      box-shadow: inset 0 470px 0 0 #9b000a;
    }
  `

  let speed = 10

  writeCode(code)

  function writeCode(code, callback){
    let n = 0
    setTimeout(function fn(){
      n += 1
      codeTag.innerHTML = code.substring(0, n)
      styleTag.innerHTML = code.substring(0, n)
      codeTag.scrollTop = codeTag.scrollHeight // 自动滚动
      if(n < code.length){
        setTimeout(fn, speed)
      } else {
        callback && callback
      }
    }, speed)
  }


  $('.actions').on('click', 'button', function(e){
    const $button = $(e.currentTarget)
    $button.addClass('active').siblings().removeClass('active')
    const dataSpeed = $button.attr('data-speed')
    switch(dataSpeed){
      case 'slow':
        speed = 100
        break
      case 'nromal':
        speed = 50
        break
      case 'fast':
        speed = 10
        break
    }
  })


}
